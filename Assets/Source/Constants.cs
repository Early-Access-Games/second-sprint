﻿using UnityEngine;

public static class Constants
{
    public const int DEFAULT_LEVEL = 2;
    public const string RESPAWN_TAG = "Respawn";
    public const string GAMEMANAGER_PATH = "Managers/Game Manager";
    public const string INPUTMANAGER_PATH = "Managers/Input Manager";
    //public const string LOADING_SCREEN = "LoadingScreen";
    public const int MAXIMUM_PLAYERS = 4;
    public const int FIXED_UPDATES_TILL_GROUNDED_CHECK = 8;
    public const int MID_GROUND = 0;
}
