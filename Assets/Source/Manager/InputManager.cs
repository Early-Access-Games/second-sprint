﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Actions
{
    MoveHorizontal,
    MoveVertical,
    Jump,
    JumpInstant,
    Fire,
    PickUp,
}

public class InputManager : MonoBehaviour
{
    public static InputManager _instance = null;
    //rely on input manager for now; there's no clean way of dealing with controllers
    //and until we get a main menu; we can't rebind keys anyway
    //final build should have a better input manager that allows changing of keys on runtime
    private Dictionary<Actions, InputKey>[] inputs = new Dictionary<Actions, InputKey>[4]
    {
        new Dictionary<Actions, InputKey> { //[player][action].value
            { Actions.MoveHorizontal, new InputKey("Horizontal_P1") },
            { Actions.MoveVertical, new InputKey("Vertical_P1") },
            { Actions.Jump, new InputKey("Jump_P1") },
            { Actions.JumpInstant, new InputKey("Jump_P1") },
            { Actions.Fire, new InputKey("Fire1_P1") },
            { Actions.PickUp, new InputKey("Pickup_P1") },
        },
        new Dictionary<Actions, InputKey> {
            { Actions.MoveHorizontal, new InputKey("Horizontal_P2") },
            { Actions.MoveVertical, new InputKey("Vertical_P2") },
            { Actions.Jump, new InputKey("Jump_P2") },
            { Actions.JumpInstant, new InputKey("Jump_P2") },
            { Actions.Fire, new InputKey("Fire1_P2") },
            { Actions.PickUp, new InputKey("Pickup_P2") },
        },
        new Dictionary<Actions, InputKey> {
            { Actions.MoveHorizontal, new InputKey("Horizontal_P3") },
            { Actions.MoveVertical, new InputKey("Vertical_P3") },
            { Actions.Jump, new InputKey("Jump_P3") },
            { Actions.JumpInstant, new InputKey("Jump_P3") },
            { Actions.Fire, new InputKey("Fire1_P3") },
            { Actions.PickUp, new InputKey("Pickup_P3") },
        },
        new Dictionary<Actions, InputKey> {
            { Actions.MoveHorizontal, new InputKey("Horizontal_P4") },
            { Actions.MoveVertical, new InputKey("Vertical_P4") },
            { Actions.Jump, new InputKey("Jump_P4") },
            { Actions.JumpInstant, new InputKey("Jump_P4") },
            { Actions.Fire, new InputKey("Fire1_P4") },
            { Actions.PickUp, new InputKey("Pickup_P4") },
        },
    };

    private void Awake()
    {
        //instantiate input manager if not already
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {
        for ( int i = 0; i < inputs.Length; i++ )
        { 
            inputs[i][Actions.MoveHorizontal].floatValue = Input.GetAxisRaw(inputs[i][Actions.MoveHorizontal].inputKey);
            inputs[i][Actions.MoveVertical].floatValue = Input.GetAxisRaw(inputs[i][Actions.MoveVertical].inputKey);
            inputs[i][Actions.Jump].boolValue = Input.GetButton(inputs[i][Actions.Jump].inputKey);
            inputs[i][Actions.JumpInstant].boolValue = Input.GetButtonDown(inputs[i][Actions.JumpInstant].inputKey);
            inputs[i][Actions.Fire].boolValue = Input.GetButtonDown(inputs[i][Actions.Fire].inputKey);
            inputs[i][Actions.PickUp].boolValue = Input.GetButtonDown(inputs[i][Actions.PickUp].inputKey);
        }
    }

    public bool GetAction(int player, Actions type)
    {
        return inputs[player][type].boolValue;
    }

    public float GetMovement(int player, Actions type)
    {
        return inputs[player][type].floatValue;
    }
}

public class InputKey
{
    public string inputKey;
    public bool boolValue;
    public float floatValue;

    public InputKey(string input)
    {
        inputKey = input;
        boolValue = false;
        floatValue = 0;
    }
}