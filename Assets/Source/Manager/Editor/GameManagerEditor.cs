﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

[CustomEditor(typeof(GameManager))]
public class GameManagerEditor : Editor
{
    private GameManager manager;
    public void OnEnable()
    {
        manager = (GameManager)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        EditorGUI.BeginChangeCheck();
        manager.currentLevel = EditorGUILayout.IntField("Current Level", manager.currentLevel);
        if (EditorGUI.EndChangeCheck())
        {
            manager.ChangeScene(manager.currentLevel);
        }
        EditorGUI.BeginChangeCheck();
        EditorGUI.BeginDisabledGroup(true);
        manager.selectedLevel = EditorGUILayout.IntField("Selected level", SceneManager.GetActiveScene().buildIndex);
        EditorGUI.EndDisabledGroup();
    }
}
