﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class PoolingManager
{
    private static readonly PoolingManagerConfig config;
    private static readonly Dictionary<PoolEnum, Pool> pools;
    private static bool poolsInitalized = false;
    
    /// <summary>
    /// Does not create any pools. Loads config, and inits PoolingManager
    /// </summary>
    static PoolingManager()
    {
        pools = new Dictionary<PoolEnum, Pool>();
        
        try
        {
            config = Resources.Load<PoolingManagerConfig>("Config/PoolingManagerConfig");
        }
        catch (Exception ex)
        {
            Debug.LogError("Pooling manager failed to find it's configuration, no pools will function. " + ex.Message);
        }
        
    }
    
    /// <summary>
    /// Must be called before any pool is accessed
    /// Reads the config asset and creates pools (the pools will
    /// create the game objects necessary)
    /// Has a safe-guard to prevent double initalization of pools.
    /// [This will most likely be called from GameManager, or a similar class]
    /// </summary>
    public static void InitPools()
    {
        if (!poolsInitalized)
        {
            foreach (var pConf in config.Pools)
                pools.Add(pConf.Pool,
                    new Pool(config.PreInitalize, pConf.PreInitAmmount, pConf.Pool, pConf.Prefab));
            
            poolsInitalized = true;
        }
        else
        {
            Debug.LogError("Pools are already initalized, but InitPools was called on PoolingManager");
        }
    }

    public static Pool GetPool(PoolEnum poolId)
    {
        if (!poolsInitalized)
            Debug.LogError("PoolingManager: Pools were never initalized before access");
        
        if (pools.ContainsKey(poolId))
            return pools[poolId];
        
        Debug.LogError("PoolingManager: Pool for poolId: " + poolId.ToString() + " does not exist," +
                       " please configure it in PoolingManagerConfig");
        return null;
    }

}
