﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class PlayerSoundController : MonoBehaviour {
    public AudioMixer masterMixer;
    public float bpm = 100;
    private int winner;
    private bool victoryPlayed;

    public AudioSource victorySource;
    public AudioClip[] victory;

    public AudioSource[] mainTrackAudioSources;

    IGameManager gm;
    PlayerStatus[] players;

    enum PlayerAnimal
    {
        Croc = 0,
        Cat = 1,
        Dog = 2,
        Bird = 3
    }

    private void Awake()
    {
        GameManager._instance.OnFightMainState += FightStart;
    }

    // Use this for initialization
    void Start ()
    {
        Debug.Log("music start");
        // Grabbing game instance and players
        gm = GameManager._instance;
        players = new PlayerStatus[gm.maxPlayers];

        // Grabbing the status of the characters
        for (int i = 0; i < gm.maxPlayers; i++)
        {
            players[i] = gm.currentPlayers[i].GetComponent<PlayerStatus>();
        }
        
        //reset mix
        masterMixer.ClearFloat("crocVol");
        masterMixer.ClearFloat("catVol");
        masterMixer.ClearFloat("dogVol");
        masterMixer.ClearFloat("birdVol");
        masterMixer.ClearFloat("percussion1");
        masterMixer.ClearFloat("percussion2");
        masterMixer.ClearFloat("percussion3");
        
        //victory state
        victoryPlayed = false;
    }

    void FightStart()
    {
        for (int i = 0; i < mainTrackAudioSources.Length; i++)
        {
            mainTrackAudioSources[i].Play();
        }
    }
	
	// Update is called once per frame
	void Update ()
	{
	    int deathCount = 0;
        // Checking for death counts
        for (int i = 0; i < gm.maxPlayers; i++)
        {
            // true = player is dead
            if (players[i].dead == true)
            {
                deathCount++;
                whoDied(i);
            }
        }
        
		if (deathCount >= 1 && !victoryPlayed)
        {
            masterMixer.SetFloat("percussion2", 0);
        } 
	    
	    if (deathCount >= 2 && !victoryPlayed)
        {
            masterMixer.SetFloat("percussion3", 0);
        }
	    
	    if (deathCount == gm.maxPlayers - 1 && !victoryPlayed)
        {
            for (int i = 0; i < gm.maxPlayers; i++)
            {
                if (players[i].dead == false)
                {
                    whoWon(i);
                    victoryPlayed = true;
                }
            }
        }
	}

    void whoDied(int player_id)
    {
        // Determining which character died, and turning off its music
        switch ((PlayerAnimal)player_id)
        {
            case PlayerAnimal.Croc:
                masterMixer.SetFloat("crocVol", -80);
                break;
            case PlayerAnimal.Cat:
                masterMixer.SetFloat("catVol", -80);
                break;
            case PlayerAnimal.Dog:
                masterMixer.SetFloat("dogVol", -80);
                break;
            case PlayerAnimal.Bird:
                masterMixer.SetFloat("birdVol", -80);
                break;
        }
    }

    void whoWon(int player_id)
    {
        // Determining who won and playing the victory music
        masterMixer.SetFloat("percussion1", -80);
        masterMixer.SetFloat("percussion2", -80);
        masterMixer.SetFloat("percussion3", -80);

        switch ((PlayerAnimal)player_id)
        {
            case PlayerAnimal.Croc:
                //Debug.Log("made it!!");
                masterMixer.SetFloat("crocVol", -80);

                //masterMixer.SetFloat("crocVictory", 0);
                victorySource.clip = victory[player_id];
                victorySource.Play();
                break;
            case PlayerAnimal.Cat:
                masterMixer.SetFloat("catVol", -80);
                //masterMixer.SetFloat("catVictory", 0);
                victorySource.clip = victory[player_id];
                victorySource.Play();
                break;
            case PlayerAnimal.Dog:
                //masterMixer.SetFloat("dogVictory", 0);
                masterMixer.SetFloat("dogVol", -80);
                victorySource.clip = victory[player_id];
                victorySource.Play();
                break;
            case PlayerAnimal.Bird:
                //masterMixer.SetFloat("birdVictory", 0);
                masterMixer.SetFloat("birdVol", -80);
                victorySource.clip = victory[player_id];
                victorySource.Play();
                break;
        }
    }

    private void OnDestroy()
    {
        GameManager._instance.OnFightMainState -= FightStart;
    }
}
