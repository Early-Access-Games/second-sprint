﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour, IGameManager
{
    public delegate void StateSwitchDelegate();
    public delegate void OnVictoryStateDelegate(int playerId);
    
    public event StateSwitchDelegate OnPregameState;
    public event StateSwitchDelegate OnCountdownState;
    public event StateSwitchDelegate OnFightMainState;
    public event OnVictoryStateDelegate OnVictoryState;
    public event StateSwitchDelegate OnDrawState;
    
    enum GameState
    {
        PreGame,
        Countdown,
        FightMain,
        Victory,
        Draw
    }

    private GameState gameState = GameState.PreGame;
    
    private const int LevelToLoadFromMainMenu = 1;
    
    public bool debug = false;
    public static GameManager _instance = null;

    //using NonSerialized to stop unity from injecting prefab values, overwriting constant values
    [NonSerialized]
    public int currentPlayers = Constants.MAXIMUM_PLAYERS;      //overridden by main menu
    [NonSerialized]
    public int selectedLevel = Constants.DEFAULT_LEVEL;      //overridden by main menu
    [NonSerialized]
    public int currentLevel = 0;

    public GameObject countdownPrefab;
    public GameObject[] playerPrefab;
    public GameObject[] respawnNodes;
    public Dictionary<int, GameObject> activePlayers = new Dictionary<int, GameObject>();
    public Dictionary<int, int> score = new Dictionary<int, int>();

    int IGameManager.currentLevel { get { return _instance.currentLevel; } }
    int IGameManager.selectedLevel { get { return _instance.selectedLevel; } }
    Dictionary<int, int> IGameManager.score { get { return _instance.score; } }
    Dictionary<int, GameObject> IGameManager.currentPlayers { get { return _instance.activePlayers; } }
    public int maxPlayers { get { return _instance.currentPlayers; } }
    
    //game state stuff
    private int winnerId = -1;

    /// <summary>
    /// Awake is called every level load
    /// </summary>
    private void Awake()
    {
        if (_instance == null)
        {
            //instance specfic initalization,
            //stuff outside this check will be run by the other instances of GameManager before they are destoryed
            _instance = this;
            
            //move onload subscription inside this _instance == null check so that
            //other game managers don't call onload (the destory happens after the first onload is called)
            //was running into a situation where onload happened twice, instantiating player duplicates
            SceneManager.sceneLoaded += OnLoad;
            DontDestroyOnLoad(gameObject);
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        InitGame();//temporary until main menu added
    }

    private void Update()
    {
        //only using the FightMainState during update
        //but leaving the code stubs to easily add the others later
        switch (gameState)
        {
            case GameState.PreGame:
                break;
            case GameState.Countdown:
                break;
            case GameState.FightMain:
                FightMainState();
                break;
            case GameState.Victory:
                break;
            case GameState.Draw:
                break;
        }
    }
    
    private void FightMainState()
    {
        int totalDead = 0;
        //keep track of kills and deaths
        for (int i = 0; i < currentPlayers; i++)
        {
            var s = activePlayers[i].GetComponent<PlayerStatus>();
            if (s != null)
                totalDead += s.dead ? 1 : 0;
        }

        if (totalDead == currentPlayers)
        {
            gameState = GameState.Draw;
            OnDrawState?.Invoke();
        }
        
        if (totalDead == currentPlayers - 1)
        {
            for (int i = 0; i < currentPlayers; i++)
            {
                var s = activePlayers[i].GetComponent<PlayerStatus>();
                if (s != null && !s.dead)
                {
                    //declare victory for this player
                    winnerId = s.playerID;
                    gameState = GameState.Victory;
                    OnVictoryState?.Invoke(winnerId);
                    
                    Debug.Log($"player {winnerId} victory");
                }
            }
        }
    }

    private void OnLoad(Scene scene, LoadSceneMode sceneMode)
    {
        Debug.Log("GM onload");
        gameState = GameState.PreGame;
        OnPregameState?.Invoke();
        
        // bad hack so GM only inits players and stuff on lava level
        if (scene.buildIndex == LevelToLoadFromMainMenu)
        {
            InitLevel();
            InitPlayers();
            
            //leave pregame state, and go to countdown
            gameState = GameState.Countdown;
            OnCountdownState?.Invoke();
            
            //start countdown prefab
            Instantiate(countdownPrefab, Vector3.zero, Quaternion.identity);
        }
    }

    public void RestartGame()
    {
        Debug.Log("Restart game");
        activePlayers = new Dictionary<int, GameObject>();
        score = new Dictionary<int, int>();
        Debug.Log("reloading scene");
        SceneManager.LoadSceneAsync(LevelToLoadFromMainMenu, LoadSceneMode.Single); 
    }
    
    //called by the animation system
    public void CountdownAnimationFinished()
    {
        gameState = GameState.FightMain;
        OnFightMainState?.Invoke();
    }

    //initializes the game: initializes values from main menu
    private void InitGame()
    {
        if (playerPrefab == null)
            DebugStop("Player prefab is not defined in the game manager.");
        if (InputManager._instance == null)
            Instantiate(Resources.Load(Constants.INPUTMANAGER_PATH));
    }
    
    //called via reflection from UI and will not be picked up as used by intelisense
    public void StartButtonListener(string actionInfo)
    {
        SceneManager.LoadScene(LevelToLoadFromMainMenu);
    }

    private void AssignID(int num, GameObject player)
    {
        player.GetComponent<PlayerController>()._playerID = num;
    }

    private void DebugStop(string str)
    {
        Debug.LogError(str);
        //Debug.Break(); no thanks, main menu doesn't have respawn nodes -_-
    }

    //read level
    private void InitLevel()
    {
        respawnNodes = GameObject.FindGameObjectsWithTag(Constants.RESPAWN_TAG); //gets respawn nodes set in level
        if (respawnNodes.Length == 0)
            DebugStop("Respawn nodes are not in this level.");
        if (!GameObject.FindGameObjectWithTag("BulletPool"))
            PoolingManager.InitPools(); //init pools
        
        currentLevel = SceneManager.GetActiveScene().buildIndex;
        if (debug)
        {
            for (int i = 0; i < respawnNodes.Length; i++)
            {
                Debug.Log("Respawn node ID: "+ respawnNodes[i].GetInstanceID());
            }
        }
    }

    //read players
    private void InitPlayers()
    {
        if (respawnNodes.Length < currentPlayers)
            Debug.LogWarning("There are less respawn nodes than players.");
        if (respawnNodes.Length > 0) //eg when in main menu, no respawn nodes, otherwise divide by zero errors
        {
            for (int i = 0; i < currentPlayers; i++) //instantiate players & assign id
            {
                GameObject player = Instantiate(playerPrefab[i]);
                AssignID(i, player);
                if (score.Count < currentPlayers || activePlayers.Count < currentPlayers)
                {
                    activePlayers.Add(i, player);
                    score.Add(i, 0);
                }

                int node = i % respawnNodes.Length; //places players on respawn nodes
                player.transform.position = respawnNodes[node].transform.position;

                if (debug)
                {
                    Debug.Log("Instantiated player: " + player);
                    Debug.Log("ID of instantiated player: " + i);
                    Debug.Log("Gameobject stored in dictionary: " + activePlayers[i]);
                    Debug.Log("Player with ID " + i + " is set to respawn node " + respawnNodes[node] +
                              " at position " + respawnNodes[node].transform.position);
                }
            }
        }
    }

    public void RandomSpawn(GameObject player)
    {
        int node = Random.Range(0, 100) % respawnNodes.Length;
        player.transform.position = respawnNodes[node].transform.position;

        if (debug)
            Debug.Log("Moved player " + player.GetComponent<PlayerController>().playerID + " to "
                + respawnNodes[node].transform.position);
    }

    public void Spawn(GameObject player, int node)
    {
        player.transform.position = respawnNodes[node].transform.position;

        if (debug)
            Debug.Log("Moved player " + player.GetComponent<PlayerController>().playerID + " to "
                + respawnNodes[node].transform.position);
    }

    public void ChangeScene(int num)
    {
        SceneManager.LoadSceneAsync(num, LoadSceneMode.Single);
    }
}