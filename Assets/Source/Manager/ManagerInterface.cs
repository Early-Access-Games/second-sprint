﻿using UnityEngine;
using System.Collections.Generic;

public interface IGameManager
{
    int currentLevel { get; }
    int selectedLevel { get; }
    int maxPlayers { get; }
    Dictionary<int, GameObject> currentPlayers { get; }
    Dictionary<int, int> score { get; }
    event GameManager.StateSwitchDelegate OnPregameState;
    event GameManager.StateSwitchDelegate OnCountdownState;
    event GameManager.StateSwitchDelegate OnFightMainState;
    event GameManager.OnVictoryStateDelegate OnVictoryState;
    event GameManager.StateSwitchDelegate OnDrawState;
}