﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

	public GameObject GameObjectToFollow;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		var p = this.transform.position;
		this.transform.position = new Vector3(
			GameObjectToFollow.transform.position.x,
			p.y,
			p.z
		);
	}
}
