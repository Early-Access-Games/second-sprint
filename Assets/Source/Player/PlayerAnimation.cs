﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// So much technical debt that I cry everytime
/// </summary>
public class PlayerAnimation : MonoBehaviour {
    private IPlayerController playerController;
    private IPlayerStatus playerStatus;
    private IPlayerItemHandler playerItem;
    private Animator animator;
    public GameObject dustRun;
    public GameObject dustRunning;
    public GameObject dustWall;
    public GameObject dustJump;
    public LayerMask raycastLayer;
    public int depth = -1;
    public bool deadIdle;
    private RaycastHit2D[] hits;

	private void Awake () {
        playerController = this.GetComponentInParent<PlayerController>();
        playerStatus = this.GetComponentInParent<PlayerStatus>();
	    playerItem = this.GetComponentInParent<PlayerItemHandler>();
        animator = GetComponent<Animator>();
	}

    // Update is called once per frame
    private void LateUpdate () {
        //don't flip sprite around when the player is dead or frozen
        if (!playerStatus.dead && !playerStatus.isFrozen)
        {
            animator.SetFloat("Facing", playerController.facing); // 1 or -1
            transform.localScale = new Vector3(playerController.facing, transform.localScale.y, transform.localScale.z);
        }

        animator.SetFloat("Moving X", Mathf.Abs(playerController.movingX)); // 0 or greater
        animator.SetFloat("Moving Y", playerController.movingY);    // +y or -y
        animator.SetBool("Grounded", playerController.grounded);            
        animator.SetBool("Walled", playerController.walled);
        animator.SetBool("Wallable", playerController.wallable);
        animator.SetBool("Idle", playerController.movingX == 0 && playerController.grounded);
        animator.SetBool("Wall Jump", playerController.wallJump);
        animator.SetBool("Dead", playerStatus.dead);
        animator.SetBool("Frozen", playerStatus.isFrozen);
        animator.SetBool("Item", playerItem.hasItem);
        animator.SetBool("Dead Idle", deadIdle);

    }
    //throw these all in an object pooler at some point
    public void playDustRun()
    {
        var effect = Instantiate(dustRun);
        effect.transform.localScale = new Vector2(effect.transform.localScale.x * playerController.facing, effect.transform.localScale.y);
        var cast = Physics2D.Raycast(this.transform.position, Vector2.down, 5F, raycastLayer).transform.GetComponent<Collider2D>();
        effect.transform.position = new Vector3(this.transform.position.x, cast.bounds.max.y, depth);
        effect.GetComponent<Animation>().Play();
    }
    public void playDustRunning()
    {
        var effect = Instantiate(dustRunning);
        effect.transform.localScale = new Vector2(effect.transform.localScale.x * playerController.facing, effect.transform.localScale.y);
        var cast = Physics2D.Raycast(this.transform.position, Vector2.down, 5F, raycastLayer).transform.GetComponent<Collider2D>();
        effect.transform.position = new Vector3(this.transform.position.x, cast.bounds.max.y, depth);
        effect.GetComponent<Animation>().Play();
    }

    public void playDustJump()
    {
        var effect = Instantiate(dustJump);
        var cast = Physics2D.Raycast(this.transform.position, Vector2.down, 5F, raycastLayer).transform.GetComponent<Collider2D>();
        effect.transform.position = new Vector3(this.transform.position.x, cast.bounds.max.y, depth);
        effect.GetComponent<Animation>().Play();
    }

    public void playDustWall()
    {
        if (playerController.facing != playerController.wallDirection)
        {
            var effect = Instantiate(dustWall);
            var cast = Physics2D.Raycast(this.transform.position, new Vector2(playerController.wallDirection, 0), 0.75F, raycastLayer);
            if (cast != null)
            {
                var collider = cast.transform.GetComponent<Collider2D>();
                effect.transform.position = new Vector3(collider.bounds.center.x - (playerController.wallDirection * collider.bounds.extents.x), this.transform.position.y, depth);
                effect.transform.localScale = new Vector2(effect.transform.localScale.x * -playerController.wallDirection, effect.transform.localScale.y);
                effect.GetComponent<Animation>().Play();
            }
        }
    }

    public void playDeathIdle()
    {
        deadIdle = true;
    }
}
