﻿//interfaces for designers
public interface IPlayerController
{
    float facing    { get; }
    float movingX   { get; }
    float movingY   { get; }
    bool  grounded  { get; }
    bool  walled    { get; }
    bool  wallable  { get; }
    int   playerID  { get; }
    bool  wallJump { get; }
    float wallDirection { get; }
}

public interface IPlayerStatus
{
    bool  dead      { get; }
    bool  invincible{ get; }
    bool  isFrozen  { get; }
    int   HP        { get; }
    
    void ApplyFreezeEffect(float freezeTime);
    void ApplyDamage(int damage);
    void ApplyDamage(int damage, int playerIdToNotHurt);
}
