﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour, IPlayerController
{
    //jump configuration
    [Header("Jump State")]
    [Tooltip("Time player can hold down the jump key")]
    public float jumpTime = 0.2F;
    [Tooltip("Starting speed of a jump")]
    public float jumpSpeedStart = 5F;

    //coyote configuration
    [Header("Special Case States")]
    [Tooltip("Multiplier on one of the rigidbodies (?)")]
    public float bouncePadding = .5F;
    [Range(0.00F, 1.00F)]
    [Tooltip("Grace period in seconds for coyote time")]
    public float coyoteTime = 1.0F;

    //wall configuration
    [Header("Wall State")]
    [Tooltip("Time in seconds until player can be rewalled after getting bounced")]
    public float walledCooldown = 1.0F;
    [Tooltip("Time player can hold down the jump key on a wall jump")]
    public float wallJumpTime = 0.2F;
    [Tooltip("Starting y speed of a wall jump")]
    public float wallJumpYSpeed = 6F;     
    [Tooltip("Fixed x speed of a wall jump")]
    public float wallJumpXSpeed = 1.2F; 
    [Tooltip("Time in seconds that wall jumps locks player horizontal control")]
    public float wallMoveLockout = 0.2F;
    [Tooltip("Gravity scale of the player when sliding down walls")]
    public float wallSlideScale = 0.5F;
    [Tooltip("How long player must hold directional key to get out of wall sliding")]
    public float wallHoldKey = 0.1F;

    [HideInInspector]
    public float wallLastDirection;
    public bool canWall;

    //jump state instances (read only)
    public JumpStateGrounded jumpStateGrounded      { get; private set; }
    public JumpStateJumping jumpStateJumping        { get; private set; }
    public JumpStateFalling jumpStateFalling        { get; private set; }
    public JumpStateBouncing jumpStateBouncing      { get; private set; }
    public WallStateWalled wallStateWalled          { get; private set; }
    public WallStateSlide wallStateSlide            { get; private set; }
    public WallStateJump wallStateJump              { get; private set; }

    [Header("Movement Settings")]
    //move configuration
    [Tooltip("Movement speed of the player")]
    public float moveSpeed = 4f;
    [Tooltip("Intensity of the acceleration property")]
    public float moveAccelerationScale = 1F;
    [Tooltip("Intensity of the acceleration property during air travel")]
    public float moveAccelerationScaleAir = 0.7F;

    //move states instances (read only)
    public MoveStateStanding moveStateStanding      { get; private set; }
    public MoveStateMoving moveStateMoving          { get; private set; }
    public MoveStateWalled moveStateWalled          { get; private set; }
    
    //when frozen
    public IPlayerControllerState jumpStateFrozen   { get; private set; }
    public IPlayerControllerState moveStateFrozen   { get; private set; }
    
    //when dead
    public IPlayerControllerState jumpStateDead   { get; private set; }
    public IPlayerControllerState moveStateDead   { get; private set; }
    
    //on gamestart
    public IPlayerControllerState jumpStateGameStart   { get; private set; }
    public IPlayerControllerState moveStateGameStart   { get; private set; }

    //jump state (read only)
    //needs to be public; animator controller relies on current information
    //and it is harder to read transitions into the special walled state
    public IPlayerControllerState currentJumpState  { get; private set; }
    public IPlayerControllerState currentMoveState  { get; private set; }
    public IPlayerControllerState previousJumpState { get; private set; }
    
    

    //components
    public Rigidbody2D rigidBody2D                  { get; private set; }
    private CapsuleCollider2D capsuleCollider;

    [Header("General Settings")]
    //wall and ground check config
    [Tooltip("What layers are ground")]
    public LayerMask whatIsGround;
    [Tooltip("What layers are wall")]
    public LayerMask whatIsWall;
    [Tooltip("What layers can be bounced from")]
    public LayerMask whatBounces;
    [Tooltip("How far to check for the ground")]
    public float groundThreshold = 0.5F;
    [Tooltip("How far to check for the wall")]
    public float wallThreshold = 0.5F;

    //wall and ground check public state
    public bool isGrounded                          { get; private set; }
    public bool isWalled                            { get; private set; }
    public bool isBounced                           { get; private set; }

    //wall and ground check private
    public RaycastHit2D[] bounceHits                { get; private set; }
    public RaycastHit2D[] wallHits                  { get; private set; }
    private RaycastHit2D[] groundHits = new RaycastHit2D[1];

    //directional variables
    public Vector2 directionFacing;
    public Vector2 directionMoving;                 

    float IPlayerController.facing                { get { return directionFacing.x; } }
    float IPlayerController.movingX               { get { return directionMoving.x; } }
    float IPlayerController.movingY               { get { return directionMoving.y; } }
    bool IPlayerController.grounded               { get { return isGrounded; } }
    bool IPlayerController.walled                 { get { return isWalled; } }
    bool IPlayerController.wallable               { get { return canWall; } }
    bool IPlayerController.wallJump               { get { return currentJumpState == wallStateJump; } }
    float IPlayerController.wallDirection         { get { return wallLastDirection; } }

    //player number
    [NonSerialized]
    public int _playerID = 0;  //assigned from game manager
    public int playerID { get {return _playerID; } } //how everything else should get the id

    //player boundary calculations
    private const float buffer = 0.01F; //buffer for calculations
    private float playerHead;
    private float playerFeet;
    private float playerRight;
    private float playerLeft;

    //debug variables, in insepector
    [Header("Debug")] 
    public bool debugVisuals = false;
    
    //player status, we watch it for dead and forzen states, which have a direct impact on how the player
    //controller will function
    public IPlayerStatus status;

    void Awake()
    {
        status = GetComponent<PlayerStatus>();
    }
    
    void Start()
    {
        //initialize array
        bounceHits = new RaycastHit2D[1];
        wallHits = new RaycastHit2D[1];

        //initialize states
        jumpStateGrounded = new JumpStateGrounded();
        jumpStateJumping = new JumpStateJumping();
        jumpStateFalling = new JumpStateFalling();
        jumpStateBouncing = new JumpStateBouncing();
        jumpStateFrozen = new JumpStateFrozen();
        jumpStateDead = new JumpStateDead();
        jumpStateGameStart = new JumpStateGameStart();
        wallStateWalled = new WallStateWalled();
        wallStateSlide = new WallStateSlide();
        wallStateJump = new WallStateJump();

        moveStateStanding = new MoveStateStanding();
        moveStateMoving = new MoveStateMoving();
        moveStateWalled = new MoveStateWalled();
        moveStateFrozen = new MoveStateFrozen();
        moveStateDead = new MoveStateDead();
        moveStateGameStart = new MoveStateGameStart();
        
        //default starting state
        currentJumpState = moveStateGameStart;
        currentMoveState = jumpStateGameStart;
        directionFacing = Vector2.right;
        canWall = true; //set false when bounced on
        
        // call state starts on default states (as they will not be called for init)
        currentJumpState.StateStart(this, null);
        currentMoveState.StateStart(this, null);

        //grab references to game object components
        rigidBody2D = this.GetComponent<Rigidbody2D>();
        capsuleCollider = GetComponent<CapsuleCollider2D>();
    }

    private IPlayerControllerState preFrozenJumpState;
    private IPlayerControllerState preFrozenMoveState;
    void Update()
    {
        //get information
        UpdateBoundaryCalculations();
        isGrounded = CheckFeet(whatIsGround, groundHits); //is grounded
        isWalled = CheckSides(whatIsWall, wallHits); //is walled
        directionMoving = rigidBody2D.velocity.normalized; //direction moving
        Vector2 pollDirection =
            new Vector2(InputManager._instance.GetMovement(playerID, Actions.MoveHorizontal), 0);
        if (pollDirection.x != 0 && pollDirection != directionFacing)
            directionFacing = pollDirection.normalized; //direction facing

        //change jump states
        previousJumpState = currentJumpState;
        var jumpState = currentJumpState.Update(this);
        jumpState = CheckOveridingJumpState(jumpState);
        if (jumpState != currentJumpState)
        {
            //wall cooldown. for when bounced
            if (!canWall)
                StartCoroutine(Cooldown(() => canWall = true, walledCooldown));

            jumpState.StateStart(this, currentJumpState);
            currentJumpState = jumpState;
        }

        //change move states
        var moveState = currentMoveState.Update(this);
        moveState = CheckOveridingMoveStates(moveState);
        if (moveState != currentMoveState)
        {
            moveState.StateStart(this, currentMoveState);
            currentMoveState = moveState;
        }

        //debug
        if (debugVisuals)
            DisplayDebugVisuals();

    }
    
    void OnGUI()
    {
        if (debugVisuals && Application.isEditor)
        {
            GUI.Box(
                new Rect(0,0,300,126), 
                "" );
            
            GUI.Label(
                new Rect(4, 0, 300, 20),
                "Jump key is down: " + InputManager._instance.GetAction(0, Actions.Jump).ToString());
            GUI.Label(
                new Rect(4, 18, 300, 20),
                "JumpState: " + currentJumpState.ToString());
            GUI.Label(
                new Rect(4, 36, 300, 20),
                "Direction: " + directionFacing.ToString());
            GUI.Label(
                new Rect(4, 52, 300, 20),
                "MoveState: " + currentMoveState.ToString());
            GUI.Label(
                new Rect(4, 70, 300, 20),
                "IsWalled: " + isWalled.ToString());
            GUI.Label(
                new Rect(4, 88, 300, 20),
                "CanWall: " + canWall.ToString());
            GUI.Label(
                new Rect(4, 106, 300, 20),
                "Is Grounded: " + isGrounded.ToString());
            
        }
    }

    void FixedUpdate()
    {
        currentJumpState.FixedUpdate(this);
        currentMoveState.FixedUpdate(this);
    }

    IPlayerControllerState CheckOveridingJumpState(IPlayerControllerState jumpState)
    {
        var overridingState = jumpState;

        if (status.isFrozen)
            overridingState = jumpStateFrozen;

        if (status.dead)
            overridingState = jumpStateDead;

        return overridingState;
    }

    IPlayerControllerState CheckOveridingMoveStates(IPlayerControllerState moveState)
    {
        var overridingState = moveState;
        
        if (status.isFrozen)
            overridingState = moveStateFrozen;

        if (status.dead)
            overridingState = moveStateDead;

        return overridingState;
    }
    
    
    //returns true/false if array if more than 0 is returned from linecasts
    //changes arrays that are passed in
    public bool CheckFeet(LayerMask mask, RaycastHit2D[] array)
    {
        return Physics2D.LinecastNonAlloc(
            new Vector2(rigidBody2D.position.x, playerFeet), //middle
            new Vector2(rigidBody2D.position.x, playerFeet - groundThreshold),
            array, mask) != 0;
    }

    //returns true/false if array if more than 0 is returned from linecasts
    //changes arrays that are passed in
    public bool CheckSides(LayerMask mask, RaycastHit2D[] array)
    {
        return Physics2D.LinecastNonAlloc(
            new Vector2(playerRight, rigidBody2D.position.y), //middle
            new Vector2(playerRight + wallThreshold, rigidBody2D.position.y),
            wallHits, whatIsWall) +
            Physics2D.LinecastNonAlloc(
            new Vector2(playerLeft, rigidBody2D.position.y), //middle
            new Vector2(playerLeft - wallThreshold, rigidBody2D.position.y),
            wallHits, whatIsWall) != 0;
    }

    //will do an action after set amount of time
    public IEnumerator Cooldown(System.Action condition, float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        condition();
    }

    private void UpdateBoundaryCalculations()
    {
        playerFeet = CalculateBottom();
        playerHead = CalculateTop();
        playerRight = CalculateRight();
        playerLeft = CalculateLeft();
    }

    private float CalculateRight()
    {
        return this.transform.position.x + (Mathf.Abs(this.transform.localScale.x) * (capsuleCollider.size.x / 2F)) - buffer;
    }

    private float CalculateLeft()
    {
        return this.transform.position.x - (Mathf.Abs(this.transform.localScale.x) * (capsuleCollider.size.x / 2F)) + buffer;
    }

    private float CalculateBottom()
    {
        return this.transform.position.y - (Mathf.Abs(this.transform.localScale.y) * (capsuleCollider.size.y / 2F)) + buffer;
    }

    private float CalculateTop()
    {
        return this.transform.position.y + (Mathf.Abs(this.transform.localScale.y) * (capsuleCollider.size.y / 2F)) - buffer;
    }
    
    private void DisplayDebugVisuals()
    {
        //Debug.DrawLine(new Vector2(_playerLeft, _playerFeet), new Vector2(_playerLeft, _playerFeet - groundThreshold), Color.red);
        Debug.DrawLine(new Vector2(rigidBody2D.position.x, playerFeet), new Vector2(rigidBody2D.position.x, playerFeet - groundThreshold), Color.red);
        //Debug.DrawLine(new Vector2(_playerRight, _playerFeet), new Vector2(_playerRight, _playerFeet - groundThreshold), Color.red);
        //Debug.DrawLine(new Vector2(_playerRight, _playerHead), new Vector2(_playerRight + wallThreshold, _playerHead), Color.red);
        Debug.DrawLine(new Vector2(playerRight, rigidBody2D.position.y), new Vector2(playerRight + wallThreshold, rigidBody2D.position.y), Color.red);
        //Debug.DrawLine(new Vector2(_playerRight, _playerFeet), new Vector2(_playerRight + wallThreshold, _playerFeet), Color.red);
        //Debug.DrawLine(new Vector2(_playerLeft, _playerHead), new Vector2(_playerLeft - wallThreshold, _playerHead), Color.red);
        Debug.DrawLine(new Vector2(playerLeft, rigidBody2D.position.y), new Vector2(playerLeft - wallThreshold, rigidBody2D.position.y), Color.red);
        //Debug.DrawLine(new Vector2(_playerLeft, _playerFeet), new Vector2(_playerLeft - wallThreshold, _playerFeet), Color.red);
    }
}