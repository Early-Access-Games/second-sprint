﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerItemHandler
{
	/// <summary>
	/// How far the player can reach to grab items
	/// </summary>
	float Range { get; }
	
	/// <summary>
	/// Where to attach the item to
	/// </summary>
	Transform AnchorPoint { get; }
	
	/// <summary>
	/// So that items can react / change behaviour based on player state
	/// Mainly used for player ID and projectiles
	/// </summary>
	IPlayerController PlayerController { get; }
	
	/// <summary>
	/// Swap out an item
	/// Item will be dropped or disposed of based on bool
	/// </summary>
	/// <param name="newItem"></param>
	/// <param name="disposeOld"></param>
	void SwapItem(Item newItem, bool disposeOld);

    bool hasItem { get; }
}

[RequireComponent(typeof(PlayerController))]
public class PlayerItemHandler : MonoBehaviour, IPlayerItemHandler
{
	//Inspect configured vars
	public Transform ItemAnchorPoint;
	public float PickupRange = 2f;
	
	//interface points
	public Transform AnchorPoint { get { return ItemAnchorPoint; } }
	public float Range { get {return PickupRange; } }
	public IPlayerController PlayerController { get; private set; }
    public bool hasItem { get { return currentItem != null; } }

    private Item currentItem;
	private InputManager inputMan;
	private IPlayerStatus status;

	void Awake()
	{
		status = GetComponent<PlayerStatus>();
	}
	
	// Use this for initialization
	void Start () {
		if (ItemAnchorPoint == null)
		{
			Debug.LogError("PlayerItemHandler: ItemAnchorPoint is not configured (ItemAnchorPoint is unassigned/null)");
			this.gameObject.SetActive(false);
		}
		
		inputMan = InputManager._instance;
		if (inputMan == null)
		{
			Debug.LogError("PlayerItemHandler: There is no InputManager in the scene");
			this.gameObject.SetActive(false);
		}

		PlayerController = this.GetComponent<PlayerController>();
		if (PlayerController == null)
		{
			Debug.LogError("PlayerItemHandler: There is no PlayerController on immediate gameobject");
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (!status.dead && !status.isFrozen)
		{
			if (inputMan.GetAction(PlayerController.playerID, Actions.PickUp))
			{
				PickupItem();
			}

			if (inputMan.GetAction(PlayerController.playerID, Actions.Fire) && currentItem != null)
			{
				currentItem.Use();
			}
		}
	}

	public void SwapItem(Item newItem, bool disposeOld)
	{
		currentItem.OnDrop();
		if (disposeOld)
		{
			Destroy(currentItem.gameObject);
		}
		
		currentItem = newItem;
		currentItem.OnPickup(this);
	}

    private void PickupItem()
	{
		Item i = null;

		var objectsHit = Physics2D.OverlapCircleAll(
			this.transform.position,
			PickupRange,
			LayerMask.GetMask("Items"));

		//item found if there's at least one object
		//hit by the circle cast
		bool itemFound = objectsHit.Length > 0;
		Collider2D closestHit = null;

		if (itemFound)
		{
			//this is initalized to make the compiler happy
			//not actually using this empty hit info
			float closestHitDistance = float.MaxValue;
			
			//grab the closest item
			foreach (Collider2D col in objectsHit)
			{
				var otherP = (Vector2)col.transform.position;
				var p = (Vector2)this.transform.position;
				var heading = otherP - p;
				var distance = heading.magnitude;
				var direction = heading / distance;

				//check if there's anything in the way of picking up the item, ignore players and items
				var lineOfSightCheck = Physics2D.Raycast(p,  direction, distance, ~LayerMask.GetMask("Player", "Items"));

				//okay when we hit nothing, or we hit something, but it's further than the item. in both cases item must be within reach
				if (lineOfSightCheck.collider == null && distance < closestHitDistance)
				{
					closestHit = col;
					closestHitDistance = distance;
				}
			}

			//is null if object is on the other side of a wall, or something else is in the way
			if (closestHit != null && closestHit.transform != null)
				i = closestHit.transform.GetComponent<Item>();
		}

		//if we found an item, make sure it's not actually held by another player
		if (i != null && !i.IsHeld)
		{
			//if we currently are holding an item, drop, and pickup the new item we found
			if (currentItem != null)
			{
				currentItem.OnDrop();
			}

			currentItem = i;
			i.OnPickup(this);
		}
	}
}
