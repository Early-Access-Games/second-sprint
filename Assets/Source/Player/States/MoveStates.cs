﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Move States //
public class MoveStateStanding : IPlayerControllerState
{
    public void StateStart(PlayerController pc, IPlayerControllerState lastState) { }

    public IPlayerControllerState Update(PlayerController pc)
    {
        if (pc.currentJumpState == pc.wallStateWalled)
            return pc.moveStateWalled;
        else if (!Mathf.Approximately(InputManager._instance.GetMovement(pc.playerID, Actions.MoveHorizontal), 0F))
            return pc.moveStateMoving;
        return pc.moveStateStanding;
    }

    public void FixedUpdate(PlayerController pc)
    {
        //no sliding, no deceleration, but respect other impulses (explosions, players pushing around)
        //remove move velocity, but respect other impulses

        //reduce horizontal velocity almost instantly if on ground
        if (pc.isGrounded)
        {
            Vector2 vel = pc.rigidBody2D.velocity;
            vel.x *= 0.1f;
            pc.rigidBody2D.velocity = vel;
        }
    }
}

public class MoveStateMoving : IPlayerControllerState
{
    //public float mult;
    bool wallJumped;
    public void StateStart(PlayerController pc, IPlayerControllerState lastState) {
        //mult = 0;
    }

    public IPlayerControllerState Update(PlayerController pc)
    {
        if (pc.currentJumpState == pc.wallStateWalled)
            return pc.moveStateWalled;
        else if (Mathf.Round(InputManager._instance.GetMovement(pc.playerID, Actions.MoveHorizontal)) == 0)
            return pc.moveStateStanding;

        return pc.moveStateMoving;
    }

    public void FixedUpdate(PlayerController pc)
    {
        Vector2 vel = pc.rigidBody2D.velocity;
        float speed = Mathf.Lerp(pc.rigidBody2D.velocity.x, pc.moveSpeed * pc.directionFacing.x, Time.deltaTime * pc.moveAccelerationScale);
        pc.rigidBody2D.velocity = new Vector2(speed, pc.rigidBody2D.velocity.y);
    }
}

//delegate all movement to wall(jump) states
//lockout movement period of time after wall jumped
public class MoveStateWalled : IPlayerControllerState
{
    float lockout;
    bool locked;
    public void StateStart(PlayerController pc, IPlayerControllerState lastState) {
        lockout = 0;
        locked = false;
    }

    public IPlayerControllerState Update(PlayerController pc)
    {
        if (pc.isGrounded || lockout <= 0 && pc.currentJumpState == pc.jumpStateFalling)
            return pc.moveStateStanding;

        if (pc.currentJumpState == pc.wallStateJump)
        {
            locked = true;
            lockout = pc.wallMoveLockout;
        }
        if (locked)
            lockout -= Time.deltaTime;

        return pc.moveStateWalled;
    }

    public void FixedUpdate(PlayerController pc) { }
}


public class MoveStateFrozen : IPlayerControllerState
{
    private IPlayerControllerState preFrozenState;
    public void StateStart(PlayerController pc, IPlayerControllerState lastState)
    {
        preFrozenState = lastState;
        Debug.Log($"prefrozenstate: {lastState}");
    }

    public IPlayerControllerState Update(PlayerController pc)
    {
        if (!pc.status.isFrozen)
        {
            return preFrozenState;
        }
        
        return this;
    }

    public void FixedUpdate(PlayerController pc)
    {
    }
}

public class MoveStateDead : IPlayerControllerState
{
    public void StateStart(PlayerController pc, IPlayerControllerState lastState)
    {
    }

    public IPlayerControllerState Update(PlayerController pc)
    {
        return this;
    }

    public void FixedUpdate(PlayerController pc)
    {
    }
}

public class MoveStateGameStart : IPlayerControllerState
{
    private bool fightStarted = false;
    public void StateStart(PlayerController pc, IPlayerControllerState lastState)
    {
        GameManager._instance.OnFightMainState += OnFightMainState;
        fightStarted = false;
    }

    public IPlayerControllerState Update(PlayerController pc)
    {
        if (fightStarted)
            return pc.moveStateStanding;
        
        return this;
    }

    public void FixedUpdate(PlayerController pc)
    {
    }

    private void OnFightMainState()
    {
        fightStarted = true;
    }
}