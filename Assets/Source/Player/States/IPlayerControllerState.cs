﻿public interface IPlayerControllerState
{
    void StateStart(PlayerController pc, IPlayerControllerState lastState);
    IPlayerControllerState Update(PlayerController pc);
    void FixedUpdate(PlayerController pc);
}