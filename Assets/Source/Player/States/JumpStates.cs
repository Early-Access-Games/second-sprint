﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using UnityEngine;

// Jump States //

public class JumpStateGrounded : IPlayerControllerState
{
    public void StateStart(PlayerController pc, IPlayerControllerState lastState) { }

    public IPlayerControllerState Update(PlayerController pc)
    {
        if (!pc.isGrounded)
            return pc.jumpStateFalling;
        else if (InputManager._instance.GetAction(pc.playerID, Actions.JumpInstant))
            return pc.jumpStateJumping;

        return pc.jumpStateGrounded;
    }

    public void FixedUpdate(PlayerController pc) { }
}

public class JumpStateFalling : IPlayerControllerState
{
    bool CanCoyote;
    public void StateStart(PlayerController pc, IPlayerControllerState lastState)
    {
        //coyote time
        if (pc.previousJumpState == pc.jumpStateGrounded)
        {
            CanCoyote = true;
            pc.StartCoroutine(pc.Cooldown(() => CanCoyote = false, pc.coyoteTime));
        }
        else
            CanCoyote = false;
    }

    public IPlayerControllerState Update(PlayerController pc)
    {
        pc.CheckFeet(pc.whatBounces, pc.bounceHits);

        if (pc.isGrounded)
            return pc.jumpStateGrounded;
        //if the returned array reference is not this player (needs testing)
        if (!ReferenceEquals(pc.bounceHits[0].collider.gameObject, pc.gameObject))
            return pc.jumpStateBouncing;
        //walled state
        #region
        //requirements to enter wall state:
        //  -pressing in direction of wall
        //  -touching wall
        //  -not grounded
        #endregion
        if (pc.isWalled)
        {
            float wallDirectionX = ((Vector2)pc.wallHits[0].transform.position - pc.rigidBody2D.position).x;

            if (Mathf.Round(wallDirectionX) == pc.directionFacing.x && pc.canWall)
                return pc.wallStateWalled;
        }
        //coyote time
        if (CanCoyote && InputManager._instance.GetAction(pc.playerID, Actions.JumpInstant))
        {
            pc.rigidBody2D.velocity = new Vector2(pc.rigidBody2D.velocity.x, 0);
            return pc.jumpStateJumping;
        }

        return pc.jumpStateFalling;
    }

    public void FixedUpdate(PlayerController pc) { }
}

public class JumpStateJumping : IPlayerControllerState
{
    float jumpStartTime;
    bool jumpKeyReleased;
    int fixedUpdateCounts;

    public void StateStart(PlayerController pc, IPlayerControllerState lastState)
    {
        fixedUpdateCounts = 0;
        jumpStartTime = Time.time;
        jumpKeyReleased = false;
    }

    public IPlayerControllerState Update(PlayerController pc)
    {   
        if (fixedUpdateCounts >= Constants.FIXED_UPDATES_TILL_GROUNDED_CHECK && pc.isGrounded)
            return pc.jumpStateGrounded;

        if (pc.rigidBody2D.velocity.y <= 0 || InputManager._instance.GetAction(pc.playerID, Actions.Jump) == false)
            return pc.jumpStateFalling;

        return pc.jumpStateJumping;
    }

    public void FixedUpdate(PlayerController pc)
    {
        fixedUpdateCounts += 1;

        if (!jumpKeyReleased)
        {
            float currentJumpTime = Mathf.Min((Time.time - jumpStartTime) / pc.jumpTime, 1f);
            float jumpAdditive = Mathf.Lerp(pc.jumpSpeedStart, 0, currentJumpTime);
            var rb = pc.rigidBody2D;
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y + jumpAdditive);
        }
    }
}

public class JumpStateBouncing : IPlayerControllerState
{
    bool bounced;
    Rigidbody2D otherBody;
    Rigidbody2D thisBody;

    public void StateStart(PlayerController pc, IPlayerControllerState lastState)
    {
        pc.canWall = false;
        bounced = false;
        otherBody = pc.bounceHits[0].rigidbody;
        thisBody = pc.rigidBody2D;
    }

    public IPlayerControllerState Update(PlayerController pc)
    {
        if (bounced)
            return pc.jumpStateFalling;
        return pc.jumpStateBouncing;
    }

    public void FixedUpdate(PlayerController pc)
    {
        if (!bounced)
        {
            otherBody.velocity = new Vector2(otherBody.velocity.x, thisBody.velocity.y);
            thisBody.velocity = new Vector2(thisBody.velocity.x, thisBody.velocity.y * -pc.bouncePadding);
            bounced = true;
        }
    }
}

public class JumpStateFrozen : IPlayerControllerState
{
    private IPlayerControllerState preFrozenJumpState;
    public void StateStart(PlayerController pc, IPlayerControllerState lastState)
    {
        preFrozenJumpState = lastState;
        Debug.Log($"Jump prefrozenstate: {lastState}");
    }

    public IPlayerControllerState Update(PlayerController pc)
    {
        if (!pc.status.isFrozen)
        {
            return preFrozenJumpState;
        }
        
        return this;
    }

    public void FixedUpdate(PlayerController pc)
    {
    }
}

public class JumpStateDead : IPlayerControllerState
{
    public void StateStart(PlayerController pc, IPlayerControllerState lastState)
    {
    }

    public IPlayerControllerState Update(PlayerController pc)
    {
        return this;
    }

    public void FixedUpdate(PlayerController pc)
    {
    }
}

public class JumpStateGameStart : IPlayerControllerState
{
    private bool fightStarted = false;
    public void StateStart(PlayerController pc, IPlayerControllerState lastState)
    {
        GameManager._instance.OnFightMainState += OnFightMainState;
        fightStarted = false;
    }

    public IPlayerControllerState Update(PlayerController pc)
    {
        if (fightStarted)
            return pc.jumpStateGrounded;
        
        return this;
    }

    public void FixedUpdate(PlayerController pc)
    {
    }

    private void OnFightMainState()
    {
        fightStarted = true;
    }
}