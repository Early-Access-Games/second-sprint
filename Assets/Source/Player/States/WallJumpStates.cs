﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

// Wall Jump States //

public class WallStateWalled : IPlayerControllerState
{
    public void StateStart(PlayerController pc, IPlayerControllerState lastState)
    {
        pc.wallLastDirection = pc.directionFacing.x;
    }

    public IPlayerControllerState Update(PlayerController pc)
    {
            pc.rigidBody2D.velocity = Vector2.zero;
            pc.rigidBody2D.gravityScale = pc.wallSlideScale;
            return pc.wallStateSlide;
    }

    public void FixedUpdate(PlayerController pc) { }
}

public class WallStateSlide : IPlayerControllerState
{
    private float holdKey;

    public void StateStart(PlayerController pc, IPlayerControllerState lastState)
    {
        holdKey = pc.wallHoldKey;
    }

    public IPlayerControllerState Update(PlayerController pc)
    {
        pc.directionFacing.x = -pc.wallLastDirection;

        if (Mathf.Round(InputManager._instance.GetMovement(pc.playerID, Actions.MoveHorizontal)) == (-1) * pc.wallLastDirection)
            holdKey -= Time.deltaTime;
        else if (holdKey != pc.wallHoldKey)
            holdKey = pc.wallHoldKey;

        //if key held in opposite direction and jumped while threshold not broken: wall jump
        if (holdKey > 0 && InputManager._instance.GetAction(pc.playerID, Actions.JumpInstant))
        {
            pc.rigidBody2D.velocity = Vector2.zero;
            pc.rigidBody2D.gravityScale = 1;
            return pc.wallStateJump;
        }
        //any other state: fall
        if (holdKey <= 0 || pc.isGrounded || !pc.isWalled)
        {
            pc.rigidBody2D.gravityScale = 1;
            return pc.jumpStateFalling;
        }

        return pc.wallStateSlide;
    }

    public void FixedUpdate(PlayerController pc) { }
}

public class WallStateJump : IPlayerControllerState
{
    bool jumpKeyReleased;
    float jumpStartTime;
    bool jumpStarted;

    public void StateStart(PlayerController pc, IPlayerControllerState lastState)
    {
        jumpKeyReleased = false;
        jumpStartTime = Time.time;
        jumpStarted = false;
    }

    public IPlayerControllerState Update(PlayerController pc)
    {
        if (InputManager._instance.GetAction(pc.playerID, Actions.Jump) == false)
            return pc.jumpStateFalling;

        return pc.wallStateJump;
    }

    public void FixedUpdate(PlayerController pc)
    {
        if (!jumpStarted)
        {
            jumpStarted = true;
            var vel = pc.rigidBody2D.velocity;
            var newVel = new Vector2(pc.wallJumpXSpeed * (-1) * pc.wallLastDirection, vel.y);
            pc.rigidBody2D.velocity = newVel;
        }
        
        if (!jumpKeyReleased)
        {
            float currentJumpTime = Mathf.Min((Time.time - jumpStartTime) / pc.wallJumpTime, 1f);
            float jumpAdditive = Mathf.Lerp(pc.wallJumpYSpeed, 0, currentJumpTime);
            var rb = pc.rigidBody2D;
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y + jumpAdditive);
        }
    }
}