﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatus : MonoBehaviour, IPlayerStatus {
    public bool dead;
    public bool invincible;
    public int playerID;
    public bool isFrozen;
    
    //readonly hp to avoid stuff messing with it
    public int HP { get { return _hp; } }
    private int _hp = 1;
    
    private PlayerController controller;

    bool IPlayerStatus.dead { get { return dead; } }
    bool IPlayerStatus.invincible { get { return invincible; } }
    bool IPlayerStatus.isFrozen { get { return isFrozen; } }

    private bool isFrozenCoutdown;
    private float frozenTimestamp;
    private float freezeTime;
    
    public void ApplyFreezeEffect(float time)
    {
        freezeTime = time;
        frozenTimestamp = Time.time;
        isFrozen = true;
        isFrozenCoutdown = true;
        Debug.Log($"Player {controller._playerID} frozennnnnn~~ for {freezeTime} seconds");
    }

    public void ApplyDamage(int damage)
    {
        Debug.Log($"Player {controller._playerID} is taking damage of: {damage}");
        AlterHP(-damage);
    }
    
    public void ApplyDamage(int damage, int playerIdToNotHurt)
    {
        if (playerID != playerIdToNotHurt)
        {
            ApplyDamage(damage);
        }
    }

    private void Awake()
    {
        controller = this.GetComponent<PlayerController>();
    }

    private void Start()
    {
        dead = false;
        invincible = false;
        playerID = controller.playerID;
    }

    private void Update()
    {
        if (isFrozenCoutdown && (frozenTimestamp + freezeTime <= Time.time))
        {
            isFrozenCoutdown = false;
            isFrozen = false;
        }
    }

    public void Kill() //temporary
    { 
        AlterHP(-_hp);
    }

    private void AlterHP(int delta)
    {
        _hp += delta;
        if (_hp <= 0)
        {
            dead = true;
            this.gameObject.layer = LayerMask.NameToLayer("PlayerDead");
        }
    }
}
