﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Video;
using Random = UnityEngine.Random;

enum LavaState
{
	Calm,
	Rising,
	Risen,
	Falling
}

public class LavaLevelLava : MonoBehaviour
{
	public float BaseYPos = -16.8f;
	public float FirstLevelYPos = -14.16f;
	public float SecondLevelYPos = -10.01f;

	//how many seconds it takes for the lava to rise, as well as fall separately
	public float TimeToRiseAndDrop = 1f;
	
	//don't bother setting min less than 2 * TimeToRiseAndDrop since it won't be respected
	public float MinTimeTillReset = 2f;
	public float MaxTimeTillReset = 4f;

	private float targetYPos = 0f;
	private LavaState state = LavaState.Calm;
	
	float timeTillReset = 0f;
	float yPosChangeStartTimestamp = 0f;
	
	void Awake()
	{
		LavaLevelEventCoordinator.EarthquakeEvent += OnEarthquake;
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		switch (state)
		{
			case LavaState.Calm:
				CalmState();
				break;
			case LavaState.Rising:
				RisingState();
				break;
			case LavaState.Risen:
				RisenState();
				break;
			case LavaState.Falling:
				FallingState();
				break;
		}
	}

	void CalmState()
	{
		//do nothing :|
	}

	void RisingState()
	{
		var t = (Time.time - yPosChangeStartTimestamp) / TimeToRiseAndDrop;
		transform.position = new Vector3(
			transform.position.x,
			Mathf.Lerp(BaseYPos, targetYPos, t),
			transform.position.z);

		if (t >= 1f)
			state = LavaState.Risen;
	}

	void RisenState()
	{
		if (Time.time >= timeTillReset)
		{
			state = LavaState.Falling;
			yPosChangeStartTimestamp = Time.time;
		}
	}

	void FallingState()
	{
		var t = (Time.time - yPosChangeStartTimestamp) / TimeToRiseAndDrop;
		transform.position = new Vector3(
			transform.position.x,
			Mathf.Lerp(targetYPos, BaseYPos, t),
			transform.position.z);

		if (t >= 1f)
			state = LavaState.Calm;
	}

	void OnEarthquake(float timeTillNextQuake)
	{
		// don't interupt an already running rising/falling lava cycle
		if (state == LavaState.Calm)
		{
			//ensure that lava resets before next quake
			timeTillReset = Random.Range(MinTimeTillReset, MaxTimeTillReset) + Time.time;

			// randomly choose between the two levels
			var i = Random.Range(0, 2);
			switch (i)
			{
				case 0:
					Debug.Log("FirstLevelYPos rolled");
					targetYPos = FirstLevelYPos;
					break;
				case 1:
					Debug.Log("SecondLevelYPos rolled");
					targetYPos = SecondLevelYPos;
					break;
			}

			state = LavaState.Rising;
			yPosChangeStartTimestamp = Time.time;
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		//kill player indiscrimently on contact
		var health = other.gameObject.GetComponent<PlayerStatus>();
		if (health != null)
		{
			Debug.Log($"Player {health.playerID} killed by lava");
			health.Kill();
		}
			
	}

	private void OnDestroy()
	{
		LavaLevelEventCoordinator.EarthquakeEvent -= OnEarthquake;
	}
}
