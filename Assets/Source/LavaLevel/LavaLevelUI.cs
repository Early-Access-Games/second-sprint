﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LavaLevelUI : MonoBehaviour
{
	public Text WinnerText;
	public Text DrawText;
	public Button RestartButton;

	void Awake()
	{
		GameManager._instance.OnPregameState += OnPregameState;
		GameManager._instance.OnVictoryState += OnVictoryState;
		GameManager._instance.OnDrawState += OnDrawState;
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnPregameState()
	{
		Debug.Log("sound pregame called");
		
		RestartButton?.gameObject?.SetActive(false);
		WinnerText?.gameObject?.SetActive(false);
		DrawText?.gameObject?.SetActive(false);
	}

	void OnVictoryState(int winnerId)
	{
		RestartButton?.gameObject?.SetActive(true);
		WinnerText?.gameObject?.SetActive(true);
	}

	void OnDrawState()
	{
		RestartButton?.gameObject?.SetActive(true);
		DrawText?.gameObject?.SetActive(true);
	}

	public void OnRestartClick()
	{
		GameManager._instance.RestartGame();
	}

	private void OnDestroy()
	{
		GameManager._instance.OnPregameState -= OnPregameState;
		GameManager._instance.OnVictoryState -= OnVictoryState;
		GameManager._instance.OnDrawState -= OnDrawState;
	}
}
