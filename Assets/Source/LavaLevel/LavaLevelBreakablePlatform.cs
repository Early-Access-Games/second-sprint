﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class LavaLevelBreakablePlatform : MonoBehaviour, IBreakableFloor {

	void Awake()
	{
		LavaLevelEventCoordinator.RegisterBreakablePlatform(this);
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnCollisionEnter2D(Collision2D other)
	{
		var s = other.gameObject.GetComponent<LavaLevelStalactite>();
		if (s != null)
			this.BreakApart();
	}

	public void BreakApart()
	{	
		Destroy(this.gameObject);
	}
}
