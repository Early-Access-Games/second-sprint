﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LavaLevelStalactite : MonoBehaviour
{
	private Rigidbody2D rb;
	private bool dropped = false;

	void Awake()
	{
		LavaLevelEventCoordinator.RegisterStalactite(this);
		rb = this.GetComponent<Rigidbody2D>();
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void Drop()
	{
		rb.bodyType = RigidbodyType2D.Dynamic;
		dropped = true;
	}

	
	private void OnCollisionEnter2D(Collision2D other)
	{
		if (dropped)
		{
			var status = other.gameObject.GetComponent<PlayerStatus>();
			if (status != null)
			{
				ContactPoint2D[] contacts = new ContactPoint2D[4];
				var c = other.GetContacts(contacts);
				for (int i = 0; i < c; i++)
				{
					//aprox 1.4 to 1.8 rads (upward angle of unit circle)
					bool normalContactPointingUpward =
						contacts[0].normal.y > 0 && (contacts[0].normal.x > -0.2 && contacts[0].normal.x < 0.2);
					if (normalContactPointingUpward)
					{
						Debug.Log($"Player {status.playerID} killed by stalactite");
						status.Kill();
						Debug.Log($"{other.gameObject.name} killed by stalactite");
					}
				}
			}
		}
	}
}
