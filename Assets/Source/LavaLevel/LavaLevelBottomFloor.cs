﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBreakableFloor
{
	void BreakApart();
}

public class LavaLevelBottomFloor : MonoBehaviour, IBreakableFloor {

	void Awake()
	{
		LavaLevelEventCoordinator.RegisterBottomFloor(this);
	}
	
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void BreakApart()
	{
		Destroy(this.gameObject);
	}
}
