﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using Random = UnityEngine.Random;

public delegate void LavaLevelEarthquake(float timeTillNextQuake);

public class LavaLevelEventCoordinator : MonoBehaviour
{
	//was going to use this to drop stalactites, but switched to using a register / call back system 
	public static LavaLevelEarthquake EarthquakeEvent;

	public float MinSecondsBetweenEarthquakes = 20f;
	public float MaxSecondsBetweenEarthquakes = 30f;
	
	private float timestampOfLastEarthquake = 0f;
	private float currentEarthquakeInterval = 0f;
	private static LavaLevelEventCoordinator _instance;

	private List<LavaLevelStalactite> unbrokenStalactites;
	private IBreakableFloor bottomFloor;
	private IBreakableFloor breakablePlatform;

	private ICameraController cam;

	private bool firstQuake = true;

	void Awake()
	{
		if (_instance != null)
			Destroy(this);
		
		_instance = this;
		timestampOfLastEarthquake = Time.time;
		currentEarthquakeInterval = Random.Range(MinSecondsBetweenEarthquakes, MaxSecondsBetweenEarthquakes);
		unbrokenStalactites = new List<LavaLevelStalactite>();
		EarthquakeEvent += OnEarthquake;
		cam = Camera.main.GetComponent<ICameraController>();
		Debug.Log($"{currentEarthquakeInterval} seconds unitl earthquake");
	}
	
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Time.time > timestampOfLastEarthquake + currentEarthquakeInterval)
		{
			timestampOfLastEarthquake = Time.time;
			currentEarthquakeInterval = Random.Range(MinSecondsBetweenEarthquakes, MaxSecondsBetweenEarthquakes);			
			EarthquakeEvent?.Invoke(currentEarthquakeInterval);
			Debug.Log($"{currentEarthquakeInterval} seconds unitl earthquake");
		}
	}

	void OnEarthquake(float timeTillNextQuake)
	{
		//Debug.Log("EARTHQUAKE!!!");
		cam.ShakeItBby();
		
		if (firstQuake)
		{
			//Random.Range max is exclusive, so we add one to include the current count as a possible result
			//in the case of the highest result: instead of dropping one of the stalactites (the roof hanging bits)
			//we'll "drop" the stalgmite (floor placed) only
			
			//note that the stalagmite will always drop into the lava in either case, but we do want 
			// to allow for the scenario where neither stalactite falls in, and only the bottom floor breaks
			var i = Random.Range(0, unbrokenStalactites.Count + 1);
			if (i != unbrokenStalactites.Count)
			{
				//one of the two stalactites fall, ground always breaks
				unbrokenStalactites[i].Drop();
				unbrokenStalactites.RemoveAt(i);
			}

			bottomFloor.BreakApart();
			firstQuake = false;
		}
		else if(unbrokenStalactites.Count > 0)
		{
			//normal case, one of the remaining stalactites fall
			//this is after the bottom floor as already broken
			
			var i = Random.Range(0, unbrokenStalactites.Count);
			unbrokenStalactites[i].Drop();
			unbrokenStalactites.RemoveAt(i);
		}
	}

	public static void RegisterStalactite(LavaLevelStalactite s)
	{
		_instance.unbrokenStalactites.Add(s);
	}

	public static void RegisterBottomFloor(IBreakableFloor f)
	{
		_instance.bottomFloor = f;
	}

	public static void RegisterBreakablePlatform(IBreakableFloor p)
	{
		_instance.breakablePlatform = p;
	}

	private void OnDestroy()
	{
		EarthquakeEvent -= OnEarthquake;
	}
}
