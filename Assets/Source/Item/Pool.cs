﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Pool
{
    public readonly PoolEnum PoolId;
    private GameObject prefab;
    private Transform poolContainerTransform;

    private List<GameObject> pooledObjs;
    private uint poolSize;
    
    /// <summary>
    /// Create a new pool, 
    /// </summary>
    /// <param name="preInit">If true, will instantiate the starting number of objects</param>
    /// <param name="preInitAmount">The number of objects that pool should start with</param>
    /// <param name="poolId">Identifies which object this pool is holding</param>
    /// <param name="prefab">The prefab the pool will instantiate and hold</param>
    public Pool(bool preInit, uint preInitAmount, PoolEnum poolId, GameObject prefab)
    {
        this.PoolId = poolId;
        this.prefab = prefab;
        
        pooledObjs = new List<GameObject>();
        poolContainerTransform = new GameObject(poolId.ToString() + "Pool").transform;
        poolContainerTransform.gameObject.tag = "BulletPool";
        GameObject.DontDestroyOnLoad(poolContainerTransform);
        if (preInit)
        {
            poolSize = 1; 
            //poolSize is incremented by CreateObject
            while (poolSize <= preInitAmount)
                CreateObject();
        }        
    }

    /// <summary>
    /// Get an existing object of the pool, if none are available creates one
    /// </summary>
    /// <returns>A GameObject that is based on the pool's prefab</returns>
    public GameObject GetObject()
    {
        foreach (var go in pooledObjs)
        {
            if (!go.activeSelf)
                return go;
        }

        return CreateObject();
    }

    /// <summary>
    /// Instantiates a gameobject, adds it to the pool, and returns it
    /// </summary>
    /// <returns>Newly created object that is tracked by pool</returns>
    private GameObject CreateObject()
    {
        var go = GameObject.Instantiate(prefab, poolContainerTransform);
        go.name = String.Format("{0} {1}", PoolId.ToString(), poolSize++);
        go.SetActive(false);
        pooledObjs.Add(go);
        return go;
    }
}
