﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(BulletDisable))]
[RequireComponent(typeof(Collider2D))]
public class BulletScript : MonoBehaviour
{
	public Weapon.WeaponType type;  //From Weapon.cs, either Melee or Ranged.
	public int OwnerPlayerID;		//ID of the owner of this hitbox.
	public float speed;				//Speed of bullet.
	public float accuracy = 0;		//Accuracy of bullet.
    public Vector2 Direction;		//The direction the hitbox travels.
    private IGameManager manager = GameManager._instance;
    
    [SerializeField] private Vector2 angle;
	private bool changeAngle = true;

	//configured when fired, do not allow inspector injection, per bullet config
	[NonSerialized] public bool canFreeze = false;
	[NonSerialized] public float freezeTime = 0f;
	[NonSerialized] public int damage = 0;

	void FixedUpdate()
	{
		//NOTE: Due to how bullets are spawned, this the current method to ensure bullets
		//      have differing angles when shot.
		if (type == Weapon.WeaponType.Ranged) 
		{	
			if (changeAngle) 
			{
				float atemp = Random.Range (-accuracy, accuracy);
				angle = new Vector2 (0, atemp);
				transform.Rotate (new Vector3 (0, 0, atemp));
				changeAngle = false;
			}
			GetComponent<Rigidbody2D>().AddForce((Direction + angle) * (speed * 2));
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		//Ignore Layer 8 (Items) and other projectiles.
		if ((other.gameObject.layer == 8) || (other.gameObject.CompareTag("Projectile")))
			return;
		else if (other.gameObject.CompareTag("Player"))
		{
			var health = other.gameObject.GetComponent<PlayerStatus>();
		    if (health.playerID != OwnerPlayerID)
		    {
			    //apply damage and effects
			    if (canFreeze)
			    	health.ApplyFreezeEffect(freezeTime);
				    
		        health.ApplyDamage(damage);
		        manager.score[OwnerPlayerID] += 1;
		    }

            else
				return; // we are collider with the owner, we want to pass through
		}

		//Disable bullet if it collides with anything else.
		gameObject.SetActive (false);
		changeAngle = true;
	}
}
