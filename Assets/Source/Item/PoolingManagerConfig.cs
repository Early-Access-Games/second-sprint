﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum PoolEnum
{
	Bullet,
	Ice,
	Railgun,
	PurpleThing,
	PinkThing
}

[CreateAssetMenu(fileName = "PoolingManagerConfig", menuName = "Config Assets / Pooling Manager Config", order = 1)]
public class PoolingManagerConfig : ScriptableObject
{
	public bool PreInitalize = true;
	
	[Serializable]
	public struct KeyVal
	{
		public PoolEnum Pool;
		public GameObject Prefab;
		public uint PreInitAmmount;
	}
	
	// if you add more than one entry for a pool then the first one in
	// the array will be selected
	public KeyVal[] Pools;
}
