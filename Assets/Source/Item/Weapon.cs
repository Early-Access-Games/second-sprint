﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Text;
using System.Timers;
using System.Xml.Schema;
using UnityEngine;
[RequireComponent(typeof(AudioSource))]

public class Weapon : Item
{	
	public enum WeaponType //A global definition of weapon types. Main use is to decide whether the hitbox moves or not.
	{
		Ranged, Melee, Launcher, Laser
	};

	public enum LaserFiringStage
	{
		Warmup, Fire, Fadeout
	};
	
	//NOTE: The launcher has not been implemented.

	[Header("Weapon Attributes")]
	public WeaponType type;						//The weapon type.
	public AudioClip soundAttack;				//The Audio Clip played when the weapon is used.
	public int DamagePerProjectile = 1;         //Because players have 1hp, this checks if bullets will kill targets.
	public float cooldown = 0.5F;				//The time it takes before item may be used again.
	public int ammo = 10; 						//Ammo available in gun.
	public int wepShots = 1;					//How many bullets spawn every time the attack key is pressed.

	[Header("Bullet Attributes")] 
	public PoolEnum BulletPool;
	public float accuracy = 0.075f;				//How much the z rotation changes when shot.
	public int speed = 10;						//How fast the bullet moves.
	public int xscale = 30;						//Scale of hitbox on x-axis.
	public int yscale = 10;						//Scale of hitbox on y-axis.
	public bool canFreeze = false;			    //Does this weapon freeze the player upon impact?
	public float freezeTime = 4f;               //How long will the player be frozen for?
	//public bool canBurn = false;				//Does this item have burn damage? (Placeholder)

	public Transform ProjectileLaunchTransform; //Position where hitbox spawns (origin of sprite).
	private float timer = 0F;					//Timer for cooldown.
	private bool fired;							//Checks if key to fire was pressed.
	private int currentOwnerPlayerID;			//ID of current parent.
	private Pool pool;							//Weapon's bullet pool.
	
	//laser state
	private bool isLaserFiring;
	private LaserFiringStage laserFiringStage;
	private Vector3 laserDirection;
	private float laserDistance;
	private Vector3 laserOriginalScale;
	[SerializeField] private SpriteRenderer laserSprite;
	private Color laserBaseColor;
	private float laserWarmupStartTimestamp;
	private float laserFireStartTimestamp;
	private float laserFadeoutStartTimestamp;
	private Vector3 laserFiredStagePosition;
	private Vector3 laserFiredStageScale;
	
	//laser config
	[SerializeField] private float MaxLaserDistance = 100f;
	[SerializeField] private int LaserDamage = 1;
	[SerializeField] private float LaserWarmupTime = 2f;
	[SerializeField] private float LaserFireTime = 0.1f;
	[SerializeField] private float LaserFadeoutTime = 0.5f;
	

	void Awake()
	{
        base.Awake();

		if (type == WeaponType.Laser)
		{
			if (laserSprite == null)
			{
				Debug.LogError("Weapon is configured as LASER, but it has no laser sprite, plz fix");
				return;
			}
			
			laserSprite.enabled = false;
			laserBaseColor = laserSprite.color;
			laserOriginalScale = laserSprite.transform.localScale;
		}
	}
	
	protected override void Start()
	{
		base.Start();
		pool = PoolingManager.GetPool(BulletPool);
	}
	
	protected override void Update()
	{
		base.Update(); //Inherit previous contents of Update()
		if (timer > 0)
		{
			//Decrement timer by one second.
			timer -= Time.deltaTime;
		}

		if (fired && timer <= 0)
		{
			//Use the current item.
			fired = false;
			timer = cooldown;
			UseWeapon();
		}

		if (isLaserFiring)
		{
			laserSprite.transform.rotation = Quaternion.identity;
			
			switch (laserFiringStage)
			{
				case LaserFiringStage.Warmup:
					{
						//prefire/warm
						//	player can move around (lasers telegraph position moves as well)
						//  raycast to see how far the beam goes
						laserSprite.enabled = true;
						laserSprite.color = new Color(laserBaseColor.r, laserBaseColor.g, laserBaseColor.b, 0.5f);
	
						laserDirection = new Vector3(playerController.facing, 0, 0);
	
						RaycastHit2D[] hitInfo = new RaycastHit2D[1];
						int hitCount = Physics2D.RaycastNonAlloc(ProjectileLaunchTransform.position, laserDirection, hitInfo, float.MaxValue,
							~LayerMask.GetMask("Player", "Items", "PlayerDead"));
	
						if (hitCount > 0)
						{
							laserDistance = Mathf.Min(MaxLaserDistance, hitInfo[0].distance);
						}
						else
						{
							laserDistance = MaxLaserDistance;
						}
	
						var t = laserSprite.transform;
						t.localScale = Vector3.one;
						t.localScale = new Vector3(
							laserDistance / transform.lossyScale.x,
							laserOriginalScale.y,
							laserOriginalScale.z);
	
						float xpos = laserDistance / 2f * laserDirection.x;
						
						t.position = new Vector3(ProjectileLaunchTransform.position.x + xpos, ProjectileLaunchTransform.position.y, ProjectileLaunchTransform.position.z);

						if (Time.time > laserWarmupStartTimestamp + LaserWarmupTime)
						{
							laserFiringStage = LaserFiringStage.Fire;
							laserFireStartTimestamp = Time.time;
							
							//make laser wide
							t.localScale = new Vector3(
								t.localScale.x,
								t.localScale.y * 3,
								t.localScale.z);
						}
					}
					break;

				case LaserFiringStage.Fire:
					{
						//fire
						//  uses the direction and size determined last frame in LaserState.Warmup 
						//	laser is locked in place, does dmg
						//  actually deal dmg (insta kill like everything else)
						
						laserSprite.color = new Color(laserBaseColor.r, laserBaseColor.g, laserBaseColor.b, 1f);
						
						Collider2D[] hitInfo = new Collider2D[4];
						int hitCount = Physics2D.OverlapBoxNonAlloc(laserSprite.transform.position, laserSprite.transform.lossyScale, 0f,
							hitInfo, LayerMask.GetMask("Player"));

						for (int i = 0; i < hitCount; i++)
						{
							var pstats = hitInfo[i].GetComponent<IPlayerStatus>();
							pstats?.ApplyDamage(LaserDamage, playerController.playerID);
						}

						if (Time.time > laserFireStartTimestamp + LaserFireTime)
						{
							laserFiringStage = LaserFiringStage.Fadeout;
							laserFadeoutStartTimestamp = Time.time;
							laserSprite.transform.SetParent(null);
						}
					}
					break;

				case LaserFiringStage.Fadeout:
					//fadeout
					//	quick lerp alpha fade
					laserSprite.color = new Color(laserBaseColor.r, laserBaseColor.g, laserBaseColor.b,
						Mathf. Lerp(1, 0, Time.time / (laserFadeoutStartTimestamp + LaserFadeoutTime)));

					if (Time.time > laserFadeoutStartTimestamp + LaserFadeoutTime)
					{
						isLaserFiring = false;
						laserSprite.enabled = false;
					}
					break;
			}
		}
	}

	override public void Use()
	{
		base.Use();
		this.Fire();
	}

	/* UseWeapon()
	 * Spawns a hitbox near the player, damaging other players who enter it.
	 * Destroys itself depending on weapon type (destroys immediately if it's a melee weapon).
	 */
	private void UseWeapon()
	{
		//shoot laser 
		if (type == WeaponType.Laser)
		{
			isLaserFiring = true;
			laserFiringStage = LaserFiringStage.Warmup;
			laserWarmupStartTimestamp = Time.time;
			laserSprite.transform.SetParent(this.transform);
			laserSprite.transform.position = Vector3.zero;
			laserSprite.transform.localScale = laserOriginalScale;
		}
		//is a regular gun thing
		else if (itemHandler != null && ammo > 0)
		{
			for (int i = 0; i < wepShots; i++) {
				//Instantiate hitbox.
				GameObject hitbox = pool.GetObject ();
				BulletScript bullet = hitbox.GetComponent<BulletScript> ();
				BulletDisable b = hitbox.GetComponent<BulletDisable> ();
		
				if (hitbox == null)
					return;
				if (bullet == null) {
					Debug.LogError ("ERROR: Hitbox is missing a BulletScript component [Weapon.cs]");
					return;
				}

				//Set parent to spawner.
				bullet.OwnerPlayerID = currentOwnerPlayerID;

				//Set type of bullet shot (freezing bullet, burning bullet, etc.)
				bullet.type = type;
				bullet.accuracy = accuracy;
				bullet.speed = speed;

				//set effects
				bullet.canFreeze = canFreeze;
				bullet.freezeTime = freezeTime;
				bullet.damage = DamagePerProjectile;

				//Set hitbox scale.
				//bullet.transform.localScale = new Vector3 (xscale, yscale, 1);

				if (type == Weapon.WeaponType.Ranged) 
					b.SecondsUntilDestroy = 2f;
				else if (type == Weapon.WeaponType.Melee)
					b.SecondsUntilDestroy = 0.1f;
				else
					Debug.LogError ("[Weapon.cs] Failed to change hitbox scale");

				hitbox.transform.position = this.ProjectileLaunchTransform.position;
				bullet.Direction.x = playerController.facing;

				//Activate hitbox.
				hitbox.SetActive (true);
			}

			//Decrement ammo count AND play attack sound.
			if (type == Weapon.WeaponType.Ranged)
				ammo--;
			AudioSource audio = GetComponent<AudioSource>();
			audio.PlayOneShot (soundAttack, 0.5f);
		}
		else
		{
			if (itemHandler != null) Debug.LogWarning("[Weapon.cs] A weapon was activated without being equipped.");
		}
	}

	public override void OnPickup(IPlayerItemHandler handler)
	{
		base.OnPickup(handler);
		
		if (itemHandler == null)
		{
			Debug.LogError("Weapon did not find component 'IPlayerItemHandler' on parent of 'anchorPoint'");
		}

		this.currentOwnerPlayerID = playerController.playerID;
	}

	public override void OnDrop()
	{
		base.OnDrop();
		this.itemHandler = null;
		
		this.currentOwnerPlayerID = -1;
	}

	public void Fire()
	{
		fired = true;
	}
}