﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDisable : MonoBehaviour
{

    public float SecondsUntilDestroy = 2f;

    private void OnEnable()
    {
        Invoke("Disable", SecondsUntilDestroy);
    }

    private void OnDisable()
    {
        CancelInvoke();
    }

    private void Disable()
    {
        gameObject.SetActive(false);
    }
}
