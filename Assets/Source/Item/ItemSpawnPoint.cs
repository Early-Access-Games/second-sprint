﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawnPoint : MonoBehaviour
{
	public GameObject ItemToSpawn;
	public float RespawnTime = 5f;
	
	private bool isItemPickedUp = false;
	private float pickupTimestamp = 0f;
	
	// Use this for initialization
	void Start () {
		SpawnItem();
	}
	
	// Update is called once per frame
	void Update () {
		if (isItemPickedUp && Time.time >= pickupTimestamp + RespawnTime)
		{
			SpawnItem();
		}
	}

	public void ItemPickedUp()
	{
		isItemPickedUp = true;
		pickupTimestamp = Time.time;
	}

	private void SpawnItem()
	{
		isItemPickedUp = false;
		var obj = Instantiate(ItemToSpawn, this.transform.position, this.transform.rotation);
		var i = obj.GetComponent<Item>();
		i.spawnPoint = this;


	}
	
}
