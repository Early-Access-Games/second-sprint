﻿using UnityEngine;


/* *** todo for rng item ***
 * need a way to talk back to player controller to swap items out of the player's hand
 * since when the player picks up this item it needs to create a put a new weapon into
 * the player's hands.
 * 
 */

public class RandomItem : Item
{
	public GameObject[] ItemPool;
	
	public override void OnPickup(IPlayerItemHandler handler)
	{
		base.OnPickup(handler);

		int itemIndex = Random.Range(0, ItemPool.Length);

		var go = GameObject.Instantiate(ItemPool[itemIndex]);
		var itemComponent = go.GetComponent<Item>();
		
		handler.SwapItem(itemComponent, true);
	}
}
