﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

//Intended to be used as a base class for other item types
//such as weapons, medkit, ect.
public class Item : MonoBehaviour
{
    public bool IsHeld { get { return isHeld; } }
    public ItemSpawnPoint spawnPoint;

    protected IPlayerItemHandler itemHandler;
    protected IPlayerController playerController;

    private Rigidbody2D rb;
    private bool isHeld = false;
    private Vector3 prefabScale;

    // Use this for initialization
    virtual protected void Awake()
    {
        prefabScale = this.transform.localScale;
        rb = this.GetComponent<Rigidbody2D>();
        if (rb == null)
        {
            Debug.LogError(string.Format("Item: {0} has no Rigidbody2d", this.name));
        }
    }
    
    // Use this for initialization
    virtual protected void Start()
    {
    }

    // Update is called once per frame
    //Empty method included so it can be overridden
    virtual protected void Update() { }

    virtual public void Use() { }

    virtual public void OnPickup(IPlayerItemHandler handler)
    {
        var t = this.gameObject.transform; 
        itemHandler = handler;
        playerController = handler.PlayerController;
        rb.simulated = false;
        isHeld = true;
        
        t.SetParent(itemHandler.AnchorPoint);
        t.rotation = Quaternion.Euler(0, playerController.facing > 0 ? 0 : 180f, 0);
        t.localPosition = Vector3.zero;

        if (spawnPoint != null)
        {
            spawnPoint.ItemPickedUp();
            spawnPoint = null;
        }
    }

    virtual public void OnDrop()
    {
        var t = this.transform;
        rb.simulated = true;
        isHeld = false;

        t.SetParent(null);
        t.localScale = prefabScale;
    }

}
