﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

public class DisplayText : MonoBehaviour
{

    public GameObject child;
    private Button play;
    public Color textColor;
    
    private void Start()
    {
        play = this.GetComponent<Button>();
    }
    private void Update()
    {
        if (play.interactable)
        {
            child.GetComponent<Text>().color = textColor;
        }
    }
}
