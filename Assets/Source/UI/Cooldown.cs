﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cooldown : MonoBehaviour {
    public Image Cooldownbar;
    public Image BarBackground;
    float cooldowntime = 10;
    float currtime;
    private CurrentAmmo currentAmmo;

    private void Awake()
    {

        currentAmmo = GetComponent<CurrentAmmo>();
    }

    // Use this for initialization
    void Start () {
        currtime = 0f;
	}
	

	// Update is called once per frame
	void Update () {
        //Cooldownbar.enabled = false;
        if ((Input.GetKeyDown(KeyCode.R)) && currentAmmo.Ammo == 0)
        {
            currtime = cooldowntime;
        }

	    if (currtime > 0)
	    {
	        Cooldownbar.enabled = true;
	        currtime -= Time.deltaTime;
	        Cooldownbar.fillAmount = currtime / cooldowntime;
	        currentAmmo.Ammo1.enabled = true;
	        currentAmmo.Ammo2.enabled = true;
	        currentAmmo.Ammo3.enabled = true;
	        currentAmmo.Ammo = 3;
        }
	}
}
