﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CharacterSelect : MonoBehaviour {

    public Image Kroc;
    public Image Bird;
    public Image Dog;
    public Image Cat;
    [SerializeField]
    private bool lockin = false;
    private int lockin_count;
    public Text Info;
    public string player;
    // Set all needed values to a preset at the start of the program.
    void Start() {
        Kroc.enabled = true;
        Bird.enabled = false;
        Dog.enabled = false;
        Cat.enabled = false;
        lockin_count = 0;
    }

    // Check to see if the player has switched characters and readies for the game then transitions to the TestLevel (currently).
    void Update() {
        if (Input.GetKeyDown(KeyCode.D) & Kroc.enabled == true & lockin == false) // D for switching characters towards the right in this order:
                                                                // Krock --> Bird --> Dog --> Cat --> Krock
        {
            Changecharacter(Kroc, Bird);
        }
        else if (Input.GetKeyDown(KeyCode.D) & Bird.enabled == true & lockin == false)
        {
            Changecharacter(Bird, Dog);
        }
        else if (Input.GetKeyDown(KeyCode.D) & Dog.enabled == true & lockin == false)
        {
            Changecharacter(Dog, Cat);
        }
        else if (Input.GetKeyDown(KeyCode.D) & Cat.enabled == true & lockin == false)
        {
            Changecharacter(Cat, Kroc);
        }
        if (Input.GetKeyDown(KeyCode.A) & Kroc.enabled == true & lockin == false) // A for switching characters towards the left in this order:
                                                                // Krock --> Cat --> Dog --> Bird --> Krock
        {
            Changecharacter(Kroc, Cat);
        }
        else if (Input.GetKeyDown(KeyCode.A) & Cat.enabled == true & lockin == false)
        {
            Changecharacter(Cat, Dog);
        }
        else if (Input.GetKeyDown(KeyCode.A) & Dog.enabled == true & lockin == false)
        {
            Changecharacter(Dog, Bird);
        }
        else if (Input.GetKeyDown(KeyCode.A) & Bird.enabled == true & lockin == false)
        {
            Changecharacter(Bird, Kroc);
        }
        if (Input.GetKeyDown(KeyCode.C) & lockin == false)      // This if statement is to check if the player has decided on a character and change there status text.
        {
            lockin = true;
            Info.text = ("READY");
        }
        else if (Input.GetKeyDown(KeyCode.V) & lockin == true) // This else if statement is to check if the player has decided that they want to change characters and change there status text.
        {
            lockin = false;
            Info.text = (player);
        }
        if (Input.GetKeyDown(KeyCode.E) & lockin == true)
        {
            SceneManager.LoadScene("TestLevel");
        }
    }
    // Purpose: To reduce the number of lines in the script. The main purpose of this funciton is to enable and disable image sprites when switching characters.
    // Parameters: sprite1 and sprite2 - sprite1 will be disabled while sprite2 will be enabled
    // Return: sprite1 and sprite2 - return the change (if they are true or false) to these two images.
    private bool Changecharacter(Image sprite1, Image sprite2)
    {
        sprite1.enabled = false;
        sprite2.enabled = true;
        return (sprite1 & sprite2);
    }
        
}
