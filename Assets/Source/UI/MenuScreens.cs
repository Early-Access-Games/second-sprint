﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuScreens : MonoBehaviour
{
    public Color colorStart = Color.red;
    public Color colorEnd = Color.green;
    public float duration = 1.0F;
    public float rate = 10F;
    public Sprite[] screens;

    private Image img;
    private int count;
    private float timer;
    private CanvasRenderer rend;

    private void Start()
    {
        img = GetComponent<Image>();
        count = 0;
        timer = 0;
        rend = GetComponent<CanvasRenderer>();

    }

    private void Update()
    {
        float lerp = Mathf.PingPong(Time.time, duration) / duration;
        rend.SetColor(Color.Lerp(colorStart, colorEnd, lerp));
        
        timer += Time.deltaTime;
        if (timer >= rate)
        {
            count++;
            if (count == screens.Length)
            {
                count = 0;
            }

            img.sprite = screens[count];
            timer = 0;
        }
    }
}
