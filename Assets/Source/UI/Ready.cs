﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ready : MonoBehaviour
{
    private InputManager im = InputManager._instance;
    private GameManager gm = GameManager._instance;
    public Button play;
    public GameObject[] players;
    public int readyCounter = 0;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	    for (int i = 0; i < 4; i++)
	    {
	        if (im.GetAction(i, Actions.Fire))
	        {
	            readyCounter++;
                players[i].GetComponent<Animator>().SetBool("Confirm", true);
	            if (readyCounter > 0)
	            {
	                play.interactable = true;
	            }
	        }
	    }
	}
}
