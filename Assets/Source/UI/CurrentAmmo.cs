﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurrentAmmo: MonoBehaviour {

    [SerializeField]
    private bool holdweapon = false; // Note: Change this to false before pushing
    public float Ammo = 0;
    public Image Ammo1;
    public Image Ammo2;
    public Image Ammo3;
    //public Image Bar;
    //public Image BarBackground;
    //float cooltime = 10;
    //float currtime;

    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update ()
    {
        //Bar.enabled = false;
        //BarBackground.enabled = false;
        if (holdweapon == true & Ammo > 0)
        {
            if ((Ammo > 2) & (Input.GetKeyDown(KeyCode.F)))
            {
                Ammo -= 1;
                Ammo3.enabled = false;
            }
            else if ((Ammo > 1) & (Ammo < 3) & (Input.GetKeyDown(KeyCode.F)))
            {
                Ammo -= 1;
                Ammo2.enabled = false;
            }
            else if ((Ammo==1) & (Input.GetKeyDown(KeyCode.F)))
            {
                Ammo -= 1;
                Ammo1.enabled = false;
                Ammo = 0;
            }
        
        }
        //else if (holdweapon == true & (Input.GetKeyDown(KeyCode.F)) & Ammo == 0 & currtime>0)
        //{
            //Bar.enabled = true;
            //BarBackground.enabled = true;
            //currtime -= Time.deltaTime;
            //Bar.fillAmount -= currtime / cooltime;
            //Ammo = 3;
            //Ammo3.enabled = true;
            //Ammo2.enabled = true;
            //Ammo1.enabled = true;
        //}
    }
}
