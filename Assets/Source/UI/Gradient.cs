﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// CAN YOU HEAR ME SAYING /NOTHING/?
/// </summary>
public class Gradient : MonoBehaviour
{
    public Color colorStart = Color.red;
    public Color colorEnd = Color.green;
    public float duration = 1.0F;
    public CanvasRenderer rend;

    void Start()
    {
        rend = GetComponent<CanvasRenderer>();
    }

    void Update()
    {
        float lerp = Mathf.PingPong(Time.time, duration) / duration;
        rend.SetColor(Color.Lerp(colorStart, colorEnd, lerp));
    }
}
