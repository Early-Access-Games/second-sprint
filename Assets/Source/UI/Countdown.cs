﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Countdown : MonoBehaviour {
    private GameManager instance = GameManager._instance;
    public void Unpause()
    {
        Debug.Log("countdown unpause");
        instance.CountdownAnimationFinished();
    }
}
