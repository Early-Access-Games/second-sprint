﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using UnityEngine;
using Random = UnityEngine.Random;

interface ICameraController
{
	void ShakeItBby();
}

public class CameraController : MonoBehaviour, ICameraController
{

	[SerializeField] float ShakeDuration = 1.5f;
	[SerializeField] private float ShakeRange = 5f;
	private Vector3 rootPosition;
	private float shakeStartTimestamp;
	[NonSerialized] private float originalZoomLevel;
	[NonSerialized] private Camera camComponent;

	[SerializeField] private float VictoryZoomLevel = 2.5f;
	[SerializeField] private float VictoryZoomTime = 0.5f;

	[NonSerialized] private bool victoryFocusMode = false;
	[NonSerialized] private Transform victoryFocusTarget;
	[NonSerialized] private float victoryTimestamp;

	void Awake()
	{
		//prevent the game from shaking at level init
		shakeStartTimestamp = -float.MaxValue;
		GameManager._instance.OnVictoryState += OnVictory;

		camComponent = GetComponent<Camera>();
		originalZoomLevel = camComponent.orthographicSize;
	}
	
	// Use this for initialization
	void Start ()
	{
		rootPosition = this.transform.position;
	}

	Vector3 velVector = new Vector3(0,0,0); 
	// Update is called once per frame
	void Update () {
		if (Time.time < shakeStartTimestamp + ShakeDuration)
		{
			//taper off the shake intensity
			var r = Mathf.Lerp(ShakeRange, 0, Time.time / (shakeStartTimestamp + ShakeDuration));
			Vector3 newPos = new Vector3(
				rootPosition.x, 
				rootPosition.y + Random.Range(-r, r), 
				rootPosition.z);

			this.transform.position = newPos;
		}
		else
		{
			this.transform.position = rootPosition;
		}

		if (victoryFocusMode)
		{
			var t = this.transform;
			camComponent.orthographicSize = Mathf.Lerp(originalZoomLevel, VictoryZoomLevel,
				(Time.time - victoryTimestamp) / VictoryZoomTime);

			Vector3.SmoothDamp(t.position, victoryFocusTarget.position,
				ref velVector, 1f);
			
			t.position = new Vector3(
				t.position.x + velVector.x,
				t.position.y + velVector.y,
				t.position.z
				);
		}
		else
		{
			camComponent.orthographicSize = originalZoomLevel;
		}
	}

	public void ShakeItBby()
	{
		shakeStartTimestamp = Time.time;
	}

	private void OnVictory(int winnerId)
	{
		victoryFocusMode = true;
		victoryFocusTarget = GameManager._instance.activePlayers[winnerId].transform;
		victoryTimestamp = Time.time;
	}
}
